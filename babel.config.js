const resolvers = {
  "@apollo": "./apollo",
  "@component": "./component",
  "@config": "./config",
  "@container": "./container",
  "@graphql": "./graphql",
  "@services": "./services",
  "@orm": "./orm",
  "@constants": "./constants",
  "@data": "./data"
}

module.exports = {
  presets: ["next/babel"],
  plugins: [
    "@babel/plugin-proposal-class-properties",
    "@babel/plugin-proposal-optional-chaining",
    ["module-resolver", { "alias": resolvers, "cwd": "babelrc" }]
  ]
}
