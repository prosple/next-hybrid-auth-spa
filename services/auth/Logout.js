import React, { useEffect } from "react"
import { getCookieName } from "./constants"
import { withAuth } from "./withAuth"

/**
 * Component: Logout.
 */
const Logout = props => {
  const { auth } = props
  const returnTo = auth.config.appUrl

  useEffect(() => {
    // Logout handler.
    if (auth.accessToken) return auth.client.logout({ returnTo })
    if (returnTo) return window.location.href = returnTo
  })

  return null
}

/**
 * Implements getInitialProps().
 */
Logout.getInitialProps = async ctx => {
  if (process.browser) return {}

  // Clear session cookie.
  const { hostname } = ctx.req
  const cookie = getCookieName({ hostname })
  if (ctx.req) ctx.req[cookie] = {}

  return {}
}

export default withAuth(Logout)
