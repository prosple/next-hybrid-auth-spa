const jwt = require("jsonwebtoken")
const jwksClient = require("jwks-rsa")
const fetch = require("isomorphic-unfetch")
const { TIME_HR_24 } = require("./constants")
const { getCookieName } = require("./constants")

const ID_TOKEN_DECODED = [
  "iss",
  "aud",
  "exp",
  "nbf",
  "iat",
  "jti",
  "azp",
  "nonce",
  "auth_time",
  "at_hash",
  "c_hash",
  "acr",
  "amr",
  "sub_jwk",
  "cnf",
  "sip_from_tag",
  "sip_date",
  "sip_callid",
  "sip_cseq_num",
  "sip_via_branch",
  "orig",
  "dest",
  "mky",
  "events",
  "toe",
  "txn",
  "rph",
  "sid",
  "vot",
  "vtm"
];

const scope = [
  "openid",
  "email",
  "profile",
  "offline_access"
]

/**
 * @typedef {Object} AuthConfig
 * @property {string} domain
 * @property {string} clientId
 * @property {string} clientSecret
 * @property {string} appUrl
 * @property {string} audience
 * @property {string} redirectUri
 */

/**
 * Fetch tokens.
 *
 * Exchanges authorization code ID and access tokens.
 *
 * @param config {AuthConfig}
 * @param code {string}
 */
const fetchTokens = async ({ config, code }) => {
  const { redirectUri, domain, clientId, clientSecret, audience } = config
  const endpoint = `https://${domain}/oauth/token`

  const options = {
    "grant_type": "authorization_code",
    "client_id": clientId,
    "client_secret": clientSecret,
    "redirect_uri": redirectUri,
    audience,
    code
  }

  const params = new URLSearchParams()
  Object.entries(options).map(([ name, value ]) => {
    params.append(name, value)
  })

  // Fetch tokens from authorisation code.
  return fetch(endpoint, { method: "POST", body: params })
    .then(response => response.json())
}

/**
 * Get JSON Web Key Set signing key handler.
 *
 * @param config {AuthConfig}
 * @param ctx {object} - Next.js context object.
 * @returns {function} - RSA signing key handler
 */
const getSigningKey = ({ config, ctx }) => {
  const { domain } = config
  const { cache } = ctx.res.locals

  const cacheKey = "auth-jwks-" + domain
  const cacheValue = cache.get(cacheKey)
  const cacheExpiry = TIME_HR_24

  // Exit early if cache value found.
  if (cacheValue) return cacheValue

  // Create JSON Web Key Set client.
  const client = jwksClient({
    jwksUri: `https://${domain}/.well-known/jwks.json`
  })

  return async (header, callback) => {
    client.getSigningKey(header.kid, (err, key) => {
      if (err) callback(err)
      const { publicKey, rsaPublicKey } = key
      const signingKey = publicKey || rsaPublicKey
      // Put signing key to cache.
      cache.put(cacheKey, signingKey, cacheExpiry)
      callback(null, signingKey)
    })
  }
}

/**
 * Decode token.
 *
 * Used to decode ID token to a Auth0 SPA compatible session.
 *
 * @url https://github.com/auth0/auth0-spa-js/blob/master/src/jwt.ts
 * @param idToken {string} - ID Token.
 * @param decoded {object} - JWT decoded access token.
 * @returns {object} Decoded token.
 */
const decodeToken = ({ idToken, decoded }) => {
  const parts = idToken.split(".")
  const [ header, payload, signature ] = parts
  const idTokenDecoded = jwt.decode(idToken)
  const headerDecoded = decoded.header
  const claims = { __raw: idToken, ...idTokenDecoded }
  const user = {}

  Object.entries(idTokenDecoded).forEach(([key, value]) => {
    if (!ID_TOKEN_DECODED.includes[key]) user[key] = value
  })

  return {
    encoded: { header, payload, signature },
    header: headerDecoded,
    claims,
    user
  }
}

/**
 * Verify JWT tokens.
 *
 * @param config {AuthConfig} - Auth config.
 * @param code {string} - Authorization code.
 * @param ctx {object} - Next.js context object.
 * @return {Promise} - Resolves token object.
 */
const verifyTokens = async ({ config, code, ctx }) => {
  const { domain, audience } = config

  // Fetch tokens.
  const tokens = await fetchTokens({ config, code })

  // Throw or exit on invalid tokens.
  if (tokens.error) throw new Error(tokens.error)

  // JWT verify options.
  // Forward slash on issuer is required.
  // @url https://stackoverflow.com/questions/49410108/auth0-authorizor-rejects-jwt-token-from-service-jwt-issuer-invalid-expected
  const options = {
    issuer: "https://" + domain + "/",
    algorithms: ["RS256"],
    complete: true,
    audience
  }

  // Verify access token, resolve decoded ID token.
  return new Promise(async (resolve, reject) => {
    await jwt.verify(
      tokens["access_token"],
      getSigningKey({ config, ctx }),
      options,
      async (error, decoded) => {
      if (error) reject(error)
      resolve({
        decodedToken: decodeToken({ decoded, idToken: tokens["id_token"] }),
        ...tokens
      })
    })
  })
}

/**
 * Builds authorisation (code) URL.
 *
 * Used to begin an Authorisation Code Flow.
 *
 * @url https://auth0.com/docs/flows/concepts/auth-code
 * @param config {AuthConfig} - Auth config.
 * @return {string} - Authorisation (code) URL.
 */
const getAuthorisationURL = ({ config }) => {
  const {
    domain,
    clientId,
    audience,
    connection,
    redirectUri
  } = config

  const options = {
    "response_type": "code",
    "client_id": clientId,
    "redirect_uri": redirectUri,
    scope: scope.join(" "),
    audience
  }

  // Undefined connection param will result in error.
  if (connection) options.connection = connection

  // Build authorisation URL.
  const endpoint = new URL(`https://${domain}/authorize`)
  Object.entries(options).map(([ name, value ]) => {
    endpoint.searchParams.append(name, value)
  })

  return endpoint.toString()
}

/**
 * Authorize user.
 *
 * Begins an Authorization Code Flow process.
 *
 * @url https://auth0.com/docs/flows/concepts/auth-code
 * @param ctx {object} - Application/Express object.
 * @param config {object} - Auth config.
 */
const authorize = ({ ctx, config }) => {
  const { hostname } = ctx.req
  const cookie = getCookieName({ hostname })

  // Reset auth session.
  ctx.req[cookie] = {}

  // Store request destination to encrypted cookie before authentication flow.
  // Used after a successful authentication to return user to original request.
  ctx.req[cookie].destination = config.appUrl

  // Mark auth session as active.
  ctx.req[cookie].isActive = true

  // Build authorisation endpoint.
  const endpoint = getAuthorisationURL({ config })

  // Begin authorisation process, redirect to authorise endpoint.
  ctx.res.redirect(302, endpoint)
}

/**
 * Verify Authorization Code from Authorization Code Flow authorisation.
 *
 * @param ctx {object} - Application/Express object.
 * @param config {AuthConfig} - Auth config.
 */
const verifyCode = async ({ ctx, config }) => {
  const { req, res } = ctx

  const { hostname } = ctx.req
  const cookie = getCookieName({ hostname })

  const instance = await verifyTokens({
    code: req.query.code,
    config,
    ctx
  })

  // Store session to encrypted cookie.
  if (instance["access_token"]) {
    const update = {
      ...req[cookie],
      ...instance,
      audience: "default",
      "client_id": config.clientId
    }

    req[cookie] = update
  }
}

/**
 * Verify authentation cookie by attempting to fetch user info.
 *
 * @param config {Object}
 * @param config.domain {String} - Auth0 application domain.
 * @return {Boolean} - True when user info is fetched.
 */
const verifyCookie = async ({ ctx, config }) => {
  const { domain } = config
  const { hostname } = ctx.req
  const cookie = getCookieName({ hostname })

  // Build user info endpoint.
  const endpoint = `https://${domain}/userinfo`

  // Load access token from encrypted cookie.
  const accessToken = ctx.req[cookie]["access_token"]

  // Exit early if access token not found.
  if (!accessToken) return false

  let hasUserInfo = false

  await fetch(endpoint, {
    method: "POST",
    headers: {
      "Authorization": "Bearer " + accessToken,
      "Content-Type": "application/json"
    }
  }).then(response => {
    if (response.ok) hasUserInfo = true
  })

  return hasUserInfo
}

/**
 * Initiate server side auth session.
 *
 * 1. Checks for portal feature.
 * 2. Checks for verification code if preset. If preset, will perform
 *    verification checks, decode and set instance to encrypted HTTP only auth
 *    session cookie.
 * 3. Verifies auth session cookie by requesting user info from Auth0. Will
 *    redirect to SSO login if authentication fails.
 *
 * NOTE: Encrypted cookies are provided by `node-client-sessions`.
 *
 * @url https://github.com/mozilla/node-client-sessions
 */
module.exports.createAuthSession = async ({ ctx, config }) => {
  // Portal authentication is a server side process only.
  if (process.browser) return

  const { hostname } = ctx.req
  const cookie = getCookieName({ hostname })

  const { isActive } = ctx.req[cookie]

  // Exit if portal feature isn't enabled.
  if (!config.features.includes("portal")) return

  // Check for Authorisation Code and process and exit.
  if (isActive && ctx.query.code) return await verifyCode({
    config: config.auth,
    ctx
  })

  // Verify authorisation cookie.
  const isVerified = await verifyCookie({ ctx, config: config.auth })

  // Authorise unidentified users. Create active auth session.
  if (!isVerified) authorize({ ctx, config: config.auth })
}
