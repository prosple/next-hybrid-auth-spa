/**
 * Auth session cookie name prefix.
 *
 * @type {string}
 */
module.exports.COOKIE_AUTH_SESSION = "auth-session-"

/**
 * Builds auth session cookie name.
 *
 * @param hostname {string} - Application hostname.
 * @returns {string} - Cookie Name.
 */
module.exports.getCookieName = ({ hostname }) => {
  return module.exports.COOKIE_AUTH_SESSION + hostname
}

/**
 * Time: Minutes: 15
 *
 * @type {number}
 */
module.exports.TIME_MN_15 = 1000 * 60 * 15

/**
 * Time: Hours: 24
 *
 * @type {number}
 */
module.exports.TIME_HR_24 = 1000 * 60 * 60 * 24

