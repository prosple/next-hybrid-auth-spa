import React, { Component } from "react"
import PropTypes from "prop-types"
import { Auth0Client } from "@auth0/auth0-spa-js"
import { isActiveAuthSession } from "./utils"
import { TIME_MN_15 } from "./constants"

export const AuthContext = React.createContext(null)

/**
 * AuthProvider.
 *
 * Exposes client, config, user and token context to consumers.
 */
class AuthProvider extends Component {

  /**
   * Prop Types.
   */
  static propTypes = {
    appUrl: PropTypes.string.isRequired,
    clientSecret: PropTypes.string,
    clientId: PropTypes.string.isRequired,
    domain: PropTypes.string.isRequired,
    redirectUri: PropTypes.string.isRequired,
    audience: PropTypes.string,
    pollInterval: PropTypes.number
  }

  /**
   * Default props.
   */
  static defaultProps = {
    pollInterval: TIME_MN_15
  }

  /**
   * State.
   */
  state = {
    accessToken: null,
    client: null,
    user: null
  }

  /**
   * Implements componentDidMount().
   */
  componentDidMount () {
    const {
      domain,
      clientId,
      pollInterval
    } = this.props

    // Create Auth0 SPA client.
    const client = new Auth0Client({
      cacheLocation: "localstorage",
      useRefreshTokens: true,
      "client_id": clientId,
      leeway: 5,
      domain
    })

    // Set client to state, refreshing context consumers.
    this.setState({ client })

    // Set poll interval.
    // Ensures refresh tokens are always fresh.
    setInterval(this.pollTokens, pollInterval)
  }

  /**
   * Implements componentDidUpdate().
   */
  componentDidUpdate(prevProps, prevState, snapshot) {
    // Provider will update on page change. Checks if auth session has been
    // initiated from callback, which requires a fresh token poll.
    if (!this.initiated && isActiveAuthSession()) {
      this.initiated = true
      this.pollTokens()
    }
  }

  /**
   * Implements componentDidMount().
   */
  componentWillUnmount() {
    clearInterval(this.pollTokens)
  }

  /**
   * Poll tokens.
   */
  pollTokens = () => {
    const { client } = this.state
    // Always use cached token.
    const options = { ignoreCache: false }
    client.getTokenSilently(options)
      .then(accessToken => {
        client.getUser()
          .then(user => this.setState({ accessToken, user }))
          .catch(error => console.log(error))
      })
      .catch(error => {
        console.log(error)
        this.setState({ accessToken: null })
      })
  }

  /**
   * Creates AuthConfig object.
   */
  getConfig () {
    const { appUrl, clientId, domain, redirectUri, audience } = this.props
    return { appUrl, clientId, domain, redirectUri, audience }
  }

  /**
   * Implements render().
   */
  render () {
    const { children } = this.props
    const { accessToken, client, user } = this.state
    const config = this.getConfig()
    const context = { accessToken, client, config, user }

    return (
      <AuthContext.Provider value={ context }>
        { children }
      </AuthContext.Provider>
    )
  }

}

export default AuthProvider
