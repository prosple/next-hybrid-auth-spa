import React, { Component } from "react"
import Router from "next/router"
import { withAuth } from "@services/auth/withAuth"
import { getCookieName } from "@services/auth/constants"
import { setActiveAuthSession } from "./utils"

class Callback extends Component {

  /**
   * Implements getInitialProps().
   */
  static async getInitialProps ({ ctx }) {
    const props = {}

    if (!process.browser) {
      const { hostname } = ctx.req
      const cookie = getCookieName({ hostname })

      // Forward active auth session to client.
      const { isActive } = ctx.req[cookie]
      if (isActive) props.authSession = ctx.req[cookie]
    }

    return props
  }

  /**
   * Implements componentDidMount().
   */
  componentDidMount () {
    this.handleCallback()
  }

  /**
   * Implements componentDidUpdate().
   */
  componentDidUpdate() {
    this.handleCallback()
  }

  /**
   * Callback handler().
   */
  handleCallback () {
    const { auth, authSession } = this.props

    // Handle SSR callback.
    // Triggered by server side authorisation request.
    // Stores SSR session into client cache.
    if (auth.client && authSession) {
      auth.client.cache.save(authSession)
      setActiveAuthSession()
      return Router.push("/")
    }

    // Handle client callback.
    // Triggered by client side authorisation requests.
    const query = window.location.search
    if (query.includes("code=") && query.includes("state=") && auth.client)
      auth.client.handleRedirectCallback()
        .then(result => Router.push("/"))
        .catch(error => console.log(error))
  }

  /**
   * Implements render().
   */
  render () {
    return null
  }

}

export default withAuth(Callback)
