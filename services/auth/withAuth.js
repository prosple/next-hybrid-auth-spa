import React, { Component } from "react"
import hoistNonReactStatics from "hoist-non-react-statics"
import { AuthContext } from "./AuthProvider"

/**
 * HOC: Attaches auth context to wrapper.
 */
export const withAuth = WrappedComponent => {
  class WithAuth extends Component {
    render () {
      const props = this.props
      return (
          <AuthContext.Consumer>
            { auth => <WrappedComponent auth={ auth } { ...props } /> }
          </AuthContext.Consumer>
      )
    }
  }

  return hoistNonReactStatics(WithAuth, WrappedComponent)
}
