import React from "react"
import { withAuth } from "./withAuth"

const User = props => {
  const { auth } = props
  return (
    <React.Fragment>
      <pre>{ JSON.stringify(auth.user, null, '\t') }</pre>
    </React.Fragment>
  )
}

export default withAuth(User)
