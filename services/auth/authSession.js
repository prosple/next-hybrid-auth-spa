const sessions = require("client-sessions")
const cache = require("memory-cache")
const { COOKIE_AUTH_SESSION } = require("./constants")

/**
 * Implement authentication session cookie middleware.
 * .
 * @param server {object} - Express instance.
 */
module.exports = ({ server }) => {
  return server.use("*", (req, res, next) => {
    res.locals.cache = cache
    return sessions({
      cookieName: COOKIE_AUTH_SESSION + req.hostname,
      secret: process.env.AUTH_SESSION_SECRET,
      duration: 1000 * 60 * 60 * 10, // 10 hours.
      cookie: {
        ephemeral: true,
        httpOnly: true
      }
    })(req, res, next)
  })
}
