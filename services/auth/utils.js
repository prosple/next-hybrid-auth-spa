import * as Cookies from "es-cookie"

/**
 * Checks local cookies for active auth session.
 *
 * @returns {string}
 */
export const isActiveAuthSession = () => {
  return Cookies.get("auth0.is.authenticated")
}

/**
 * Manually sets active auth session.
 * Replicates activation method found in Auth0 SPA.
 */
export const setActiveAuthSession = () => {
  return Cookies.set("auth0.is.authenticated", "true")
}
