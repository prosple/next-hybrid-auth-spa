import React from "react"
import { withAuth } from "./withAuth"

/**
 * Handler: Click.
 *
 * @param props {object} - Component props.
 */
const handleClick = ({ props }) => {
  const { client, config } = props.auth
  client.loginWithRedirect({
    "redirect_uri": config.redirectUri
  })
}

/**
 * Component: Login.
 */
const Login = props => {
  return (
    <button onClick={ () => handleClick({ props })}>
      Login
    </button>
  )
}

export default withAuth(Login)
