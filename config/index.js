import getConfig from "next/config"
const { publicRuntimeConfig } = getConfig()

export default {
  features: [
    "portal"
  ],
  auth: publicRuntimeConfig.auth
}
