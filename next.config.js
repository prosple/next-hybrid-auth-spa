
module.exports = {
  publicRuntimeConfig: {
    auth: {
      appUrl: process.env["AUTH_APP_URL"],
      domain: process.env["AUTH_DOMAIN"],
      clientId: process.env["AUTH_CLIENT_ID"],
      clientSecret: process.env["AUTH_CLIENT_SECRET"],
      audience: process.env["AUTH_AUDIENCE"],
      redirectUri: process.env["AUTH_REDIRECT_URI"],
    }
  }
}
