# next-auth-spa

An server-side and client hybrid Auth0 application with Next.js.

# Overview

The [Auth0 SPA](https://github.com/auth0/auth0-spa-js) SDK is used in conjuction
with [Auth0 REST APIs](https://auth0.com/docs/api/info) to achieve universal
authentication.

## Flow

### "Portal" Type Applications

A server and client side authentication process.

1. Unauthenticated user lands on any page. A encrypted, HTTP only auth
   session cookie is created (if not present). User is redirected to
   authorisation URL to start a Auth0 "code" flow. Any previous auth session
   cookie is emptied.
2. User successfully authenticates with universal login screen and is redirected
   to callback page.
3. During the server side process, application verifies code by fetching
   tokens against supplied code (`/oauth/token`). Access tokens are then
   verified against Auth0 signing keys and the application secret. If
   successful, tokens are decoded and a session is stored manually in the auth
   session cookie. Application then attempts to get user info to ensure access.
   During the client side process, the auth session is stored in local storage.
   The user is redirected to their original destination. If this fails, process
   restarts to step 1.
4. The application creates a poll that periodically refreshes the user session
   with `[getTokenSilently](https://auth0.github.io/auth0-spa-js/interfaces/gettokensilentlyoptions.html)`.

### "Non-Portal" Type Applications

A client side only authentication process.

1. Unauthenticated user lands on any page and manually selects login. This is
   handled with `[loginWithRedirect](https://auth0.github.io/auth0-spa-js/classes/auth0client.html#loginwithredirect)`.
2. User successfully authenticates with universal login screen and is redirected
   to callback page.
3. During the client side process, the application verifies and stores the auth
   session with `[handleRedirectCallback](https://auth0.github.io/auth0-spa-js/classes/auth0client.html#handleredirectcallback)`.
   The session is marked as "active". The user is redirected to their original
   destination.
4. The application creates a poll that periodically refreshes the user session
   with `[getTokenSilently](https://auth0.github.io/auth0-spa-js/interfaces/gettokensilentlyoptions.html)`.

NOTE: [JSON Web Key Sets](https://auth0.com/docs/tokens/concepts/jwks) used for
verifying tokens are cached locally for 24 hours.
NOTE: Server side auth session is decoded to match [decoding algorithm used
by Auth0 SPA](https://github.com/auth0/auth0-spa-js/blob/master/src/jwt.ts#L39).
NOTE: An auth session includes Auth0 client, user, config and token data. These
are stored in local storage, auth session cookie as well as browser memory
(React context](https://reactjs.org/docs/context.html)).
NOTE: Both server and client sessions are initiated with the "code" flow.
NOTE: Client side authentication implements [Refresh Token Rotation](https://auth0.com/docs/tokens/concepts/refresh-token-rotation).
NOTE: Server side authentication is only required for "portal" type applications.
