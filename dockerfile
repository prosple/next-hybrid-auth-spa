FROM node:12.16-alpine
RUN chown -R node:node /srv
USER node
WORKDIR /srv
COPY . /srv
ENV PATH /srv/node_modules/.bin:$PATH
