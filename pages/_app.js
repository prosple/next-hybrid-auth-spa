import React from "react"
import App, { Container } from "next/app"
import config from "@config"
import AuthProvider from "@services/auth/AuthProvider"

class AppWapper extends App {

  /**
   * Implements getInitialProps().
   */
  static async getInitialProps ({ Component, ctx }) {
    let pageProps = {}

    if (!process.browser) {
      // Initiate auth session.
      const { createAuthSession } = require("@services/auth/server")
      await createAuthSession({ ctx, config })
    }

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx, config })
    }

    return { pageProps }
  }

  /**
   * Implements render().
   */
  render () {
    const { Component, pageProps } = this.props

    return (
      <Container>
        <AuthProvider { ...config.auth }>
          <Component { ...pageProps } config={ config } />
        </AuthProvider>
      </Container>
    )
  }

}

export default AppWapper
