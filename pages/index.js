import React from "react"
import Login from "@services/auth/Login"
import User from "@services/auth/User"
import Link from "next/link"

const Index = () => {
  return (
    <React.Fragment>
      <Login />
      <Link href="/logout">
        <a>Logout</a>
      </Link>
      <User />
    </React.Fragment>
  )
}

export default Index
